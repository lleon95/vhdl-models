-- 
-- Counters
-- By: Luis G Leon-Vega
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity counter_4_bit is
  port (
    clk : in std_logic;
    rst : in std_logic;
    a   : out std_logic_vector(3 downto 0)
  );
end counter_4_bit;

architecture counter_4_bit_arch of counter_4_bit is

  signal count :std_logic_vector (3 downto 0);
  
begin
  
  COUNTER: process(clk, rst)
  begin
    if falling_edge(clk) and rst = '0' then
      count <= count + 1;
    else
      count <= std_logic_vector(to_unsigned(0,count'length));
    end if;
  end process COUNTER;
  
  a <= count;
end architecture counter_4_bit_arch;
