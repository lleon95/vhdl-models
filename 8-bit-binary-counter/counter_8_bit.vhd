-- 
-- Counters
-- By: Luis G Leon-Vega
-- Warning: include the simple counter_4_bit
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity counter_8_bit is
  port (
    clk : in std_logic;
    rst : in std_logic;
    a   : out std_logic_vector(7 downto 0)
  );
end counter_8_bit;

architecture counter_8_bit_arch of counter_8_bit is

  signal carry : std_logic;
  signal lsbus : std_logic_vector(3 downto 0);
  signal msbus : std_logic_vector(3 downto 0);

  component counter_4_bit
    port (
      clk : in std_logic;
      rst : in std_logic;
      a : out std_logic_vector(3 downto 0)
    );
  end component;
  
begin
  
  -- Assign carry as the MSB of the LSBus
  carry <= lsbus(3);
  CNT1 : counter_4_bit port map (clk => clk, rst => rst, a => lsbus);
  CNT2 : counter_4_bit port map (clk => carry, rst => rst, a => msbus);
  -- Join the 8-bit output
  a(3 downto 0) <= lsbus;
  a(7 downto 4) <= msbus; 

end architecture counter_8_bit_arch;
